import React, { useEffect, useState } from "react";
import { todos as todosList } from "./todos";
import { v4 as uuid } from "uuid";

function App() {
  const [todos, setTodos] = useState(todosList);
  const [inputText, setInputText] = useState("");
  const newId = uuid();

  const handleAdd = (event) => {
    if (event.key === "Enter") {
      const newTodo = {
        userId: 1,
        id: newId,
        title: inputText,
        completed: false,
      };

      const newTodos = [...todos, newTodo];
      setTodos(newTodos);
      setInputText("");
    }
  };

  const handleDelete = (id) => {
    const newTodos = todos.filter((todo) => todo.id !== id);
    setTodos(newTodos);
  };

  const clearCompleted = (e) => {
    const newTodos = todos.filter((todo) => todo.completed === false);
    setTodos(newTodos);
  };

  const handleCheck = (id) => {
    const index = todos.findIndex((todo) => todo.id === id);
    const newTodos = [...todos];
    newTodos[index].completed = !newTodos[index].completed;
    /***The line below does exactly what the above line does ***/
    //newTodos[index].completed === false ? (newTodos[index].completed = true) : (newTodos[index].completed = false);
    setTodos(newTodos);
  };

  return (
    <section className="todoapp">
      <header className="header">
        <h1>todos</h1>
        <input
          className="new-todo"
          placeholder="What needs to be done?"
          value={inputText}
          onChange={(event) => setInputText(event.target.value)}
          onKeyDown={(event) => handleAdd(event)}
          autoFocus
        />
      </header>
      <TodoList todos={todos} setTodos={setTodos} handleDelete={handleDelete} handleCheck={handleCheck} />
      <footer className="footer">
        <span className="todo-count">
          <strong>0</strong> item(s) left
        </span>
        <button className="clear-completed" onClick={clearCompleted}>
          Clear completed
        </button>
      </footer>
    </section>
  );
}

function TodoItem(props) {
  return (
    <li key={props.todo.id} className={props.todo.completed ? "completed" : ""}>
      <div className="view">
        <input
          className="toggle"
          type="checkbox"
          checked={props.todo.completed}
          onChange={(e) => {
            props.handleCheck(props.todo.id);
          }}
        />
        <label>{props.todo.title}</label>
        <button
          className="destroy"
          onClick={(e) => {
            props.handleDelete(props.todo.id);
          }}
        />
      </div>
    </li>
  );
}

function TodoList(props) {
  return (
    <section className="main">
      <ul className="todo-list">
        {props.todos.map((todo) => (
          <TodoItem todo={todo} handleDelete={props.handleDelete} handleCheck={props.handleCheck} />
        ))}
      </ul>
    </section>
  );
}

export default App;
